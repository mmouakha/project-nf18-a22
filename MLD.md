Espece (

#nom : varchar PK

CHECK (nom IN {”félin”, “canidé”, “reptile”, “rongeur”, “oiseau”, “autre”})

)

Patient (

#id : int PK

nom : varchar NOT NULL, 

dateNaiss : integer[4] NOT NULL, 

num_puce : long integer UNIQUE, 

num_pass: long integer UNIQUE,

espece⇒Espece NOT NULL

)

<aside>
ℹ️ On a ici choisi une transformation de l’héritage par classe fille car l’heritage est exclusif :

</aside>

Procedure  (

#num : integer PK, 

date : date NOT NULL, 

heure : hour NOT NULL, 

patient⇒Patient NOT NULL

description : text

CHECK(UNIQUE(date, heure, titre, patient))

)

<aside>
ℹ️ *Le triplet (patient, date, heure) doit être unique étant donné qu’un même animal ne peut subir plusieurs procédures en même temps.*

</aside>

Traitement  (

#num : integer PK, 

date : date NOT NULL,

heure : hour NOT NULL,

dateDebut : date NOT NULL, 

duree : integer

patient⇒Patient NOT NULL,

CHECK(datEDebut >= curdate)

METHODE : duree = dateFin - dateDebut

)

<aside>
ℹ️ *On vérifie que la date de début soit supérieure à la date courante. On ne peut pas prescrire un traitement à un animal dans le passé.*

</aside>

Medicament (

#nom_mol : varchar PK,

description : text

)

Autoriser (

#espece ⇒ Espece, 

#medicament ⇒ Medicament

PK (espece, medicament)

)

Prescrire (

#medicament⇒Medicament, 

#traitement⇒Traitement,

veterinaire⇒ Personnel NOT NULL

quantité : integer,

PK (medicament, traitement),

CHECK (personnel.poste = ‘veterinaire’),

CHECK (Traitement.patient.espece IN (SELECT espece FROM Autoriser WHERE (Autoriser.medicament=Prescrire.medicament)))

)

Contrainte :`Prescrire.medicament NOT NULL PROJECTION(Traitement,num) =  PROJECTION(Prescrire, traitement)` 

<aside>
ℹ️ *La prescription d’un traitement se fait seulement par un vétérinaire. On vérifie aussi que les médicaments prescrits font bien parti de la liste des médicaments autorisés pour l’espèce de cet animal.*

</aside>

Mesure ( 

#num integer PK, 

date : date NOT NULL, 

heure : hour NOT NULL, 

taille : integer, 

poids : integer,

patient⇒Patient NOT NULL,

CHECK(NOT NULL (taille, poids))

)

<aside>
ℹ️ *Le couple (taille, poids) ne doit pas être nul : au moins l’une des mesures doit être saisi.*

</aside>

Resultat_analyse ( 

#num integer PK, 

date : date NOT NULL, 

heure : hour NOT NULL,

patient⇒Patient NOT NULL,

resultat : text NOT NULL

)

<aside>
ℹ️ *Le texte de resultat est un lien vers un document électronique.*

</aside>

Contrainte : `INTERSECTION (PROJECTION(Resultat_analyse,num), PROJECTION(Observation, id), PROJECTION (Procedure,id), PROJECTION(Mesure,num), PROJECTION (Traitement, num)) = {}`

Specialiser (

#espece ⇒ Espece, 

#personnel ⇒ Personnel, 

PK (espece, personnel),

) 

<aside>
ℹ️ On a ici choisi une transformatin de l’héritage par classe fille car l’heritage est exclusif :

</aside>

Personnel (

#id : integer PK,

nom : varchar NOT NULL,

prenom : varchar NOT NULL,

dateNaiss : date NOT NULL,                       

adresse⇒Adresse  NOT NULL ,

num_tel : long integer{10} UNIQUE NOT NULL,

poste : enum{”veterinaire”, “assistant”} NOT NULL,

)

Contrainte : `INTERSECTION (PROJECTION(Personnel,id), PROJECTION(Client,id)) = {}`

Client (

#id : integer PK,

nom : varchar NOT NULL,

prenom : varchar NOT NULL,

dateNaiss : date NOT NULL,

adresse⇒Adresse  NOT NULL ,

num_tel : long integer{10} UNIQUE NOT NULL,

)

Observation (

#num : integer PK,

date : date NOT NULL,

heure : hour NOT NULL,

description : text,

personnel ⇒ Personnel NOT NULL,

patient ⇒ Patient NOT NULL,

)

Etre_proprietaire (

#client ⇒ Client,

#patient ⇒ Patient,

dateDebut : date NOT NULL,

dateFin : date,

CHECK (dateFin>dateDebut OR dateFin = NULL)

PK (client, patient)

)

Contrainte :`PROJECTION(Patient,id) =  PROJECTION(etre_proprietaire,patient) AND PROJECTION (Client,id) = PROJECTION (etre_propriétaire, client)`

Suivre (

#patient ⇒ Patient,

#personnel ⇒ Personnel,

debut : date NOT NULL,

fin : date NOT NULL,

PK(patient, personnel),

CHECK (UNIQUE(patient, personnel, debut, fin))

CHECK (fin>debut OR fin = NULL)

CHECK (personnel.poste = veterinaire)

<aside>
ℹ️ //condition pour que les dates ne se chevauchent pas pour un même couple (patient, personnel)//

</aside>

)

Contraintes : `PROJECTION(Suivre ,personnel) =  PROJECTION(Personnel,id)` 

<aside>
ℹ️ *Le suivi se fait seulement par des vétérinaires. Un même animal ne peut pas être suivi par le même vétérinaire au même moment dont l’unicité.*

</aside>

Adresse (

#num_rue : integer 

#nom_rue : varchar 

#ville : varchar 

PK(num_rue, nom_rue, ville)

)

<aside>
ℹ️ *Nous avons créé les classes Adresse pour éviter d’avoir de case multivalué (une des conditions de la première forme normale)*

</aside>
